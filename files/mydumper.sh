#!/bin/bash
#
# Logic Backup MySQL data using MyDumper tool
# Daniel Guzman Burgos <daniel.guzman.burgos@percona.com>
#
# Downloaded from https://raw.githubusercontent.com/nethalo/backup-scripts/master/mydumper.sh
# 
# There were quite TWO CHANGES (by Michaël de Groot) compared to the original:
# - Configuration parameters are now loaded from /etc/mydumper.cnf
# - Bug fix that it reads $rootPath/$i/metadata instead of $rootPath/$i/.metadata
# - Added the history feature, implemented in destructor and delete job
# Please make sure you reimplement those changes when fetching a new version.

clear

set -o pipefail

# Load configuration file
source /etc/mydumper.cnf


# Function definitions

function sendAlert () {
        if [ -e "$errorFile" ]
        then
                alertMsg=$(cat $errorFile)
                echo -e "${alertMsg}" | mailx -s "[$HOSTNAME] ALERT MyDumper backups" "${email}"
        fi
}

function historyQuery() {
    if [ "$mydumper_history_enabled" = "yes" ]; then
        mysql --host="$mydumper_history_host" --user="$mydumper_history_user" \
            --password="$mydumper_history_password" $mydumper_history_schema \
            -e "$1"

        verifyExecution "$?" "Cannot query history database"
    fi
}

function markHistoryDeleted() {
    sql="UPDATE mydumper_history SET deleted_at=NOW() WHERE hostname=\"${hostName}\" AND target_host=\"${remoteHost}\" AND backupPath=\"$1\""
    historyQuery "$sql"

}

function migrateHistoryTable() {
    sql="CREATE TABLE IF NOT EXISTS mydumper_history ( \
            hostname VARCHAR(100) NOT NULL, \
            target_host VARCHAR(100) NOT NULL, \
            backup_path VARCHAR(100) NOT NULL, \
            created_at TIMESTAMP NOT NULL, \
            result ENUM('success', 'failed') not null, \
            size_in_mb int unsigned not null, \
            deleted_at TIMESTAMP NULL, \
            PRIMARY KEY (hostname, target_host, backup_path) \
        )"
    
    historyQuery "$sql"
}

function storeHistory() {

    migrateHistoryTable

    if [ -e "$errorFile" ]; then result="failed";
    else result="success"; fi

    hostname=`/bin/hostname`
    backup_size=`/usr/bin/du -sm ${backupPath}|awk '{print $1}'`

    sql="INSERT INTO mydumper_history( \
            hostname,         target_host,       backup_path,         result,        size_in_mb, created_at \
        ) VALUES ( \
            \"${hostname}\", \"${remoteHost}\", \"${backupPath}\", \"${result}\", ${backup_size}, NOW()) \
            ON DUPLICATE KEY UPDATE created_at=NOW()"

    historyQuery "$sql"
}

function destructor () {
        sendAlert
            storeHistory
        rm -f "$lockFile" "$errorFile"
}

# Setting TRAP in order to capture SIG and cleanup things
trap destructor EXIT INT TERM

function verifyExecution () {
        local exitCode="$1"
        local mustDie=${3-:"false"}
        if [ $exitCode -ne "0" ]
        then
                msg="[ERROR] Failed execution. ${2}"
                echo "$msg" >> ${errorFile}
                if [ "$mustDie" == "true" ]; then
                        exit 1
                else
                        return 1
                fi
        fi
        return 0
}

function setLockFile () {
        if [ -e "$lockFile" ]; then
                trap - EXIT INT TERM
                verifyExecution "1" "Script already running. $lockFile exists"
                sendAlert
                rm -f "$errorFile"
                exit 2
        else
                touch "$lockFile"
        fi
}

function logInfo (){

        echo "[$(date +%y%m%d-%H:%M:%S)] $1" >> $logFile
}

function verifyMysqldump () {
	which mysqldump &> /dev/null
        verifyExecution "$?" "Cannot find mysqldump tool" true
        logInfo "[OK] Found 'mysqldump' bin"
}

function runMysqldump () {
	
	verifyMysqldump

	out=$(mkdir -p $backupPath)
	verifyExecution "$?" "Can't create backup dir $backupPath. $out" true
	logInfo "[Info] $backupPath exists"
	local schemas=$(mysql -u${mysqlUser} -h${remoteHost} -p${mysqlPassword} --port=${3306} -N -e"select schema_name from information_schema.schemata where schema_name not in ('information_schema', 'performance_schema')")
	if [ ! -z "$schemas" ]; then
		for i in $schemas; do
			out=$(mysqldump -u${mysqlUser} -h${remoteHost} -p${mysqlPassword} --port=${3306} -d $i | gzip > $backupPath/${i}_schema.sql.gz 2>&1)
			verifyExecution "$?" "Problems dumping schema for db $i. $out"
			logInfo "[OK] Dumping $i schema with mysqldump"
		done
		return
	fi

	verifyExecution "1" "While getting schemas, this happened: $schemas"
}

function verifyMydumperBin () {
	which mydumper &> /dev/null
	verifyExecution "$?" "Cannot find mydumper tool" true
	logInfo "[OK] Found 'mydumper' bin"
}

function runMydumper () {

	verifyMydumperBin
	logInfo "[Info] Dumping data with MyDumper.....start"
	out=$(mydumper --user=${mysqlUser} --password=${mysqlPassword} --outputdir=${backupPath} --host=${remoteHost} --port=${mysqlPort} --threads=${numberThreads} --compress --kill-long-queries --no-schemas --verbose=3 &>> $logFile)
	verifyExecution "$?" "Couldn't execute MyDumper. $out" true

	logInfo "[Info] Dumping data with MyDumper.....end"
}

function removeOldBackup () {

	rootPath=$(dirname $backupPath 2>&1)
	verifyExecution "$?" "Couldn't find backup path. $rootPath" true

	pushd $rootPath &> /dev/null
	daysAgo=$(date -d "$daily days ago" +%s)
	weeksAgo=$(date -d "$weekly weeks ago" +%s)
	
	logInfo "[Info] Removing old backups"
	for i in $(ls -1); do
		day=$(cat $rootPath/$i/metadata | grep Finished | awk -F": " '{print $2}' | awk '{print $1}' 2>&1)
		verifyExecution "$?" "Couldn't find $rootPath/$i/metadata file. $day"

		backupTs=$(date --date="$day" +%s)

		# Remove weekly backups older than $weekly
                if [ $weeksAgo -gt $backupTs  ]; then
                        out=$(rm -rf $rootPath/$i 2>&1)
			verifyExecution "$?" "Error removing $rootPath/${i}. $out"
                        markHistoryDeleted "$i"
                        logInfo "  [OK] Removed $rootPath/$i weekly backup"
                fi
		
		# Do not remove daily backup if its from Sunday
		weekDay=$(date --date="$day" +%u)
		if [ $weekDay -eq 7 ]; then
			continue;
		fi
		
		# Remove daily backups older than $daily
		if [ $daysAgo -gt $backupTs  ]; then
			out=$(rm -rf $rootPath/$i 2>&1)
			verifyExecution "$?" "Error removing $rootPath/${i}. $out"
            markHistoryDeleted "$i"
			logInfo "  [OK] Removed $rootPath/$i daily backup"
		fi

	done
	
	popd &> /dev/null
}

setLockFile
runMysqldump
runMydumper
removeOldBackup
