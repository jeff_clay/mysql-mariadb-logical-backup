# MySQL or MariaDB logical backup

This role installs _mydumper_, a multithreaded logical backup tool. It also installs an open-source wrapper script. 

# Monitoring

**Automatic monitoring is not enough. Please verify your backups at least twice the retention time.**<br />
Use this query for your monitoring, to monitor the size of the last backup created in the last 36 hours:<br />
`select SUM(size_in_mb) AS backup_size FROM (SELECT 0 AS size_in_mb UNION DISTINCT (SELECT size_in_mb FROM dba.mydumper_history WHERE created_at > NOW() - INTERVAL 36 HOUR AND result='success' AND hostname=(SELECT @@hostname) AND target_host IN ('localhost', '127.0.0.1', (SELECT @@hostname)) order by created_at DESC LIMIT 1)) a;`<br />
After 36 hours of the last backup failing, the size will drop. This query monitors both failed backups and backup size.

# Todo
- CentOS installation
- Improve security (does not have to run as root)
- Restore documentation
- Role documentation
- Better testing

# Pull requests

PR's are very welcome, this role is actively maintained (As of the time of writing, July 2019)!